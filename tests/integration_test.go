package test

import (
	"encoding/json"
	"hipchat/responses"
	_ "hipchat/routers"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
)

func init() {
	_, file, _, _ := runtime.Caller(1)
	apppath, _ := filepath.Abs(filepath.Dir(filepath.Join(file, ".."+string(filepath.Separator))))
	beego.TestBeegoInit(apppath)
}

// TestEmptyPost is a sample to run an endpoint test
func TestEmptyPost(t *testing.T) {
	r, _ := http.NewRequest("POST", "/v1/message_analysis", nil)
	r.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)

	beego.Trace("testing", "TestPost", "Code[%d]\n%s", w.Code, w.Body.String())

	Convey("Subject: Test Message Analysis Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("It has the correct payload", func() {
			var response responses.MessageAnalysisResponse
			json.Unmarshal(w.Body.Bytes(), &response)
			So(response.Emoticons, ShouldBeEmpty)
			So(response.Links, ShouldBeEmpty)
			So(response.Mentions, ShouldBeEmpty)
		})
	})
}

func TestNonEmptyPost(t *testing.T) {
	r, _ := http.NewRequest("POST", "/v1/message_analysis", strings.NewReader(`{"RawValue":"this is a message http://google.com (allthethings) @rahul-monga"}`))
	r.Header.Set("Content-Type", "application/json")
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)

	beego.Trace("testing", "TestPost", "Code[%d]\n%s", w.Code, w.Body.String())

	Convey("Subject: Test Message Analysis Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("It has the correct non empty payload", func() {
			var response responses.MessageAnalysisResponse
			json.Unmarshal(w.Body.Bytes(), &response)
			So(response.Emoticons, ShouldNotBeEmpty)
			So(response.Links, ShouldNotBeEmpty)
			So(response.Mentions, ShouldNotBeEmpty)
		})
		Convey("It has the values", func() {
			var response responses.MessageAnalysisResponse
			json.Unmarshal(w.Body.Bytes(), &response)
			So(response.Emoticons[0], ShouldEqual, "allthethings")
			So(response.Links[0]["url"], ShouldEqual, "http://google.com")
			So(response.Links[0]["title"], ShouldEqual, "Google")
			So(response.Mentions[0], ShouldEqual, "rahul-monga")
		})
	})

}
