package responses

type MessageAnalysisResponse struct {
	Mentions  []string
	Emoticons []string
	Links     []map[string]string
}
