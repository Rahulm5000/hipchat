# HipChat Message Analyzer

### To run:

cd into the root project directory and then:

``docker-compose up -d``

(or if you already have your go env set up, then just bee run)

### Example hitting the endpoint via curl:
```curl -d '{"RawValue":"this is my message @rahul-monga (allthethings2) (notthisonetoooolong) http://yahoo.com http://google.com "}' -H "Content-Type: application/json" -X POST http://localhost:8080/v1/message_analysis```

or you can hit the service with something like Postman

### To stop the service:

``docker-compose stop``


### To run tests (need to go get convey before hand):

```go test -v ./tests```


### My thought process while implementing
I decided to give this a go with golang. I thought to myself that i could do this faster with ruby or java, but hey, i wanted to use this as a learning opportunity.

I literally googled ```golang rest api``` to see how others go about building a web service on go and what the best practices are. It came down to using go's standard library to build out the end point, or to use a framework. I decided to use beego, thinking it would save me some time as it would provide me with some boiler plate code.

My first step was to familiarize myself with the codebase, and how files are laid out. I then dockerized the app, as i love having my dev env easy to set up. Second, i flushed out some models and services that i would need to take care of the tasks outlined in the spec. Third was to actually implement the skeleton code i laid out in the previous step, and make sure everything works end to end. Again, this took a little longer as i've never done any go development before, so i spent some time reading documentation and what things are available in the go stdlib (its a lot :)). My last step was to write some unit tests.

Some things i might change/re implement given more time:

- try to parallelize the get requests i'm making to the URLs found in the message. This part of the implementation can hang if we have many URLs, or if we have a bad url and have to wait for a timeout to continue. Doing some quick reading, it seems like go's way of doing concurrency is through goroutines and channels.

- have better error handling around failed GET requests; now i'm just skipping the bad url and not populating the title key in the returned json payload

- try to find a better regex for URL; the one i have is nasty.

- have more tests at each level, rather than just having a single file doing integration tests. the motivation for having different service files was that each of them could be tested independently, and without having to go through the flow of making http requests.
