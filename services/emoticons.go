package services

import (
	"hipchat/models"
	"regexp"
)

// GetEmoticons returns all emoticons in message
func GetEmoticons(message models.Message) []string {
	results := []string{}
	r := regexp.MustCompile("\\((?P<Emoticon>[\\d\\w]{1,15})\\)")

	for _, match := range r.FindAllStringSubmatch(message.RawValue, -1) {
		results = append(results, match[1])
	}

	return results
}
