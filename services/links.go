package services

import (
	"hipchat/models"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
)

// GetLinks returns all links in message
func GetLinks(message models.Message) []map[string]string {
	results := []map[string]string{}
	r := regexp.MustCompile("(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?")

	for _, match := range r.FindAllString(message.RawValue, -1) {
		results = append(results, getURLInfo(match))
	}

	return results
}

func getURLInfo(url string) map[string]string {
	info := map[string]string{"url": url}
	r := regexp.MustCompile(`<title>(?P<Title>.*)</title>`)

	resp, err := http.Get(url)
	if err != nil {
		log.Printf(err.Error())
		return info
	}

	defer resp.Body.Close()
	htmlBody, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Printf(err.Error())
		return info
	}

	htmlString := string(htmlBody)
	info["title"] = r.FindStringSubmatch(htmlString)[1]

	return info
}
