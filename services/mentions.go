package services

import (
	"hipchat/models"
	"regexp"
)

// GetMentions return all the @ mentions in the message
func GetMentions(message models.Message) []string {
	results := []string{}
	r := regexp.MustCompile("@(?P<Mention>[\\w-]+)")

	for _, match := range r.FindAllStringSubmatch(message.RawValue, -1) {
		results = append(results, match[1])
	}

	return results
}
