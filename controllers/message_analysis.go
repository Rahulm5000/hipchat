package controllers

import (
	"hipchat/models"
	"encoding/json"
  "hipchat/responses"
	"github.com/astaxie/beego"
  "hipchat/services"
  "log"
)

// Operations about Users
type MessageAnalysisController struct {
	beego.Controller
}

// @Title AnalyzeMessage
// @Description analyze a message
// @Param	body		body 	models.Message	true		"body for messaget"
// @Success 200 {object} models.Message
// @Failure 403 body is empty
// @router / [post]
func (controller *MessageAnalysisController) Post() {
  var newMessage models.Message
  json.Unmarshal(controller.Ctx.Input.RequestBody, &newMessage)
  log.Printf("Message:", newMessage.RawValue)

  var messageResponse = responses.MessageAnalysisResponse{}
  messageResponse.Emoticons = services.GetEmoticons(newMessage)
  messageResponse.Mentions = services.GetMentions(newMessage)
  messageResponse.Links = services.GetLinks(newMessage)

  controller.Data["json"] = messageResponse
	controller.ServeJSON()
}
